package repositories;

import domain.EnumerationValue;
import domain.Person;
import domain.User;

public interface RepositoryCatalog {

	public IUserRepository getUsers();
	public IRepository<User> getUser();
	public IRepository<Person> getPerson();
	public IRepository<EnumerationValue> getEnumerationValue();
	public void commit();
}