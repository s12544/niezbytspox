package repositories;
import java.util.List;

import domain.User;

public interface IUserRepository extends IRepository<User>{

	public List<User> withUsername(String username);
	public List<User> withUsername(int userId);
}